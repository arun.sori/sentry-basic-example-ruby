require "sentry-ruby"

Sentry.init do |config|
  config.dsn =  ENV["SENTRY_DSN"]
  config.debug = true
  config.breadcrumbs_logger = [:sentry_logger, :http_logger]
  # To activate performance monitoring, set one of these options.
  # We recommend adjusting the value in production:
  config.traces_sample_rate = 1.0
  # or
  config.traces_sampler = lambda do |context|
    0.5
  end
  # INSECURE: Only disable ssl verification when using self-signed certs
  ssl_verification = true
  insecure_ok = ENV["INSECURE_OK"].to_s.downcase
  if insecure_ok == "true"
    ssl_verification = false
  end
  config.transport.ssl_verification = ssl_verification
end


class RubySdkTestError < StandardError
    def message
      "Hello from Ruby Sentry SDK"
    end
end

begin
  raise RubySdkTestError
  rescue RubySdkTestError => exception
    Sentry.capture_exception(exception)
end

#begin
#  1 / 0
#rescue ZeroDivisionError => exception
#  Sentry.capture_exception(exception)
#end

#Sentry.capture_message('foo')

